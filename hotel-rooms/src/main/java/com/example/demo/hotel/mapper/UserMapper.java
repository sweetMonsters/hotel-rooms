package com.example.demo.hotel.mapper;

import com.example.demo.hotel.DTO.UserDTO;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

//@Mapper
public interface UserMapper {

    List<UserDTO> getAllUsers();

    UserDTO getUserById(Integer id);

    void createUser(UserDTO userDTO);

    void updateUser(UserDTO userDTO);

    void deleteUser(Integer id);


    UserDTO findByUsername(String account);

}
