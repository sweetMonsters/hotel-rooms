import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Index from '@/components/Index'
import User from '@/components/User'
import Rooms from '@/components/Rooms'
import Registration from '@/components/Registration'
import Order from '@/components/Order'
import RoomStatus from '@/components/RoomStatus'
import Views from '@/components/Views'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect:'login'
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      redirect:'/index',
      children: [
        {
          path: '/users',
          name: 'User',
          component: User
        },
        {
          path: '/index',
          name: 'Index',
          component: Index,
        },
        {
          path: '/views',
          name: 'Views',
          component: Views,
        },
        {
          path: '/rooms',
          name: 'Rooms',
          component: Rooms,
        },
        {
          path: '/registration',
          name: 'Registration',
          component: Registration,
        },
        {
          path: '/order',
          name: 'Order',
          component: Order,
        },
        {
          path: '/roomstatus',
          name: 'RoomStatus',
          component: RoomStatus,
        }
      ]

    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    }
  ]
})
