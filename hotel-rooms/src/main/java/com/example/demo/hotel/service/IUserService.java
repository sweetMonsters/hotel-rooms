package com.example.demo.hotel.service;

import com.example.demo.hotel.DTO.UserDTO;
import com.example.demo.hotel.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IUserService {

     List<UserDTO> getAllUsers();
     UserDTO getUserById(Integer id);
     UserDTO createUser(UserDTO userDTO);
     UserDTO updateUser(Integer id, UserDTO userDTO);
     void deleteUser(Integer id);


     public boolean validateUser(String account, String password);

}
