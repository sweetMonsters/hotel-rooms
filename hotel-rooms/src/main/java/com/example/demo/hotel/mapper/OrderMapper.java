package com.example.demo.hotel.mapper;

import com.example.demo.hotel.DTO.OrderDTO;

import java.util.List;

//@Mapper
public interface OrderMapper {

    List<OrderDTO> getAllOrder();

    OrderDTO getOrderById(Integer id);

    void createOrder(OrderDTO orderDTO);

    void updateOrder(OrderDTO orderDTO);

    void deleteOrder(Integer id);

}
