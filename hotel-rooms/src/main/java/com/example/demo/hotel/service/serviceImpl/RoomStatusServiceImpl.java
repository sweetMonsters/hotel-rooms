package com.example.demo.hotel.service.serviceImpl;

import com.example.demo.hotel.DTO.RoomStatusDTO;
import com.example.demo.hotel.mapper.RoomStatusMapper;
import com.example.demo.hotel.service.IRoomStatusService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomStatusServiceImpl implements IRoomStatusService {
    private final RoomStatusMapper roomStatusMapper;

    public RoomStatusServiceImpl(RoomStatusMapper roomStatusMapper) {
        this.roomStatusMapper = roomStatusMapper;
    }

    @Override
    public List<RoomStatusDTO> getAllRoomStatus() {
        return roomStatusMapper.getAllRoomStatus();
    }

    @Override
    public RoomStatusDTO getRoomStatusById(Integer id) {
        return roomStatusMapper.getRoomStatusById(id);
    }

    @Override
    public RoomStatusDTO createRoomStatus(RoomStatusDTO roomStatusDTO) {
        roomStatusMapper.createRoomStatus(roomStatusDTO);
        return roomStatusDTO;
    }

    @Override
    public RoomStatusDTO updateRoomStatus(Integer id, RoomStatusDTO roomStatusDTO) {
        roomStatusDTO.setId(id);
        roomStatusMapper.updateRoomStatus(roomStatusDTO);
        return roomStatusDTO;
    }

    @Override
    public void deleteRoomStatus(Integer id) {
        roomStatusMapper.deleteRoomStatus(id);
    }



    @Override
    public void updateRoomStatusByRoomNumber(RoomStatusDTO roomStatusDTO) {
        roomStatusMapper.updateRoomStatusByRoomNumber(roomStatusDTO);
    }

}
