package com.example.demo.hotel.controller;

import com.example.demo.hotel.DTO.OrderDTO;
import com.example.demo.hotel.service.IOrderService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
@CrossOrigin(origins = "http://localhost:8080/", methods = {RequestMethod.GET, RequestMethod.POST})
public class OrderController {
    private final IOrderService orderService;

    public OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * getAll
     * @return
     */
    @GetMapping("/get")
    public List<OrderDTO> getAllOrder() {
        return orderService.getAllOrder();
    }

    /**
     * getByID
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public OrderDTO getOrderById(@PathVariable Integer id) {
        return orderService.getOrderById(id);
    }


    /**
     * add
     * @param orderDTO
     * @return
     */
    @PostMapping("/add")
    public OrderDTO createOrder(@RequestBody OrderDTO orderDTO) {
        return orderService.createOrder(orderDTO);
    }

    /**
     * update
     * @param id
     * @param orderDTO
     * @return
     */
    @PutMapping("/update/{id}")
    public OrderDTO updateOrder(@PathVariable Integer id, @RequestBody OrderDTO orderDTO) {
        return orderService.updateOrder(id, orderDTO);
    }

    /**
     * delete
     * @param id
     */
    @DeleteMapping("/delete/{id}")
    public void deleteOrder(@PathVariable Integer id) {
        orderService.deleteOrder(id);
    }

}
