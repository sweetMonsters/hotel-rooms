package com.example.demo.hotel.controller;

import com.example.demo.hotel.DTO.RoomsDTO;
import com.example.demo.hotel.service.IRoomsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rooms")
@CrossOrigin(origins = "http://localhost:8080/", methods = {RequestMethod.GET, RequestMethod.POST})
public class RoomsController {
    private final IRoomsService roomsService;

    public RoomsController(IRoomsService roomsService) {
        this.roomsService = roomsService;
    }

    /**
     * getAll
     * @return
     */
    @GetMapping("/get")
    public List<RoomsDTO> getAllRooms() {
        return roomsService.getAllRooms();
    }

    /**
     * getByID
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public RoomsDTO getRoomsById(@PathVariable Integer id) {
        return roomsService.getRoomsById(id);
    }


    /**
     * add
     * @param roomsDTO
     * @return
     */
    @PostMapping("/add")
    public RoomsDTO createRooms(@RequestBody RoomsDTO roomsDTO) {
        return roomsService.createRooms(roomsDTO);
    }

    /**
     * update
     * @param id
     * @param roomsDTO
     * @return
     */
    @PutMapping("/update/{id}")
    public RoomsDTO updateRooms(@PathVariable Integer id, @RequestBody RoomsDTO roomsDTO) {
        return roomsService.updateRooms(id, roomsDTO);
    }

    /**
     * delete
     * @param id
     */
    @DeleteMapping("/delete/{id}")
    public void deleteRooms(@PathVariable Integer id) {
        roomsService.deleteRooms(id);
    }

}
