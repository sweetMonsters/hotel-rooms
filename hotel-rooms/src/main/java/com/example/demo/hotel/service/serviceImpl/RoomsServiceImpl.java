package com.example.demo.hotel.service.serviceImpl;

import com.example.demo.hotel.DTO.RoomsDTO;
import com.example.demo.hotel.mapper.RoomsMapper;
import com.example.demo.hotel.service.IRoomsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomsServiceImpl implements IRoomsService {
    private final RoomsMapper roomsMapper;

    public RoomsServiceImpl(RoomsMapper roomsMapper) {
        this.roomsMapper = roomsMapper;
    }

    @Override
    public List<RoomsDTO> getAllRooms() {
        return roomsMapper.getAllRooms();
    }

    @Override
    public RoomsDTO getRoomsById(Integer id) {
        return roomsMapper.getRoomsById(id);
    }

    @Override
    public RoomsDTO createRooms(RoomsDTO roomsDTO) {
        roomsMapper.createRooms(roomsDTO);
        return roomsDTO;
    }

    @Override
    public RoomsDTO updateRooms(Integer id, RoomsDTO roomsDTO) {
        roomsDTO.setId(id);
        roomsMapper.updateRooms(roomsDTO);
        return roomsDTO;
    }

    @Override
    public void deleteRooms(Integer id) {
        roomsMapper.deleteRooms(id);
    }

}
