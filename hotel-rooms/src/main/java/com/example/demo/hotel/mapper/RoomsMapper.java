package com.example.demo.hotel.mapper;

import com.example.demo.hotel.DTO.RoomsDTO;
import com.example.demo.hotel.DTO.UserDTO;

import java.util.List;

//@Mapper
public interface RoomsMapper {

    List<RoomsDTO> getAllRooms();

    RoomsDTO getRoomsById(Integer id);

    void createRooms(RoomsDTO roomsDTO);

    void updateRooms(RoomsDTO roomsDTO);

    void deleteRooms(Integer id);

}
