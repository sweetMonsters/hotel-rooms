package com.example.demo.hotel.service.serviceImpl;
import com.example.demo.hotel.DTO.RegistrationDTO;
import com.example.demo.hotel.mapper.RegistrationMapper;
import com.example.demo.hotel.service.IRegistrationService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationServiceImpl implements IRegistrationService {
    private final RegistrationMapper registrationMapper;

    public RegistrationServiceImpl(RegistrationMapper registrationMapper) {
        this.registrationMapper = registrationMapper;
    }

    @Override
    public List<RegistrationDTO> getAllRegistration() {
        return registrationMapper.getAllRegistration();
    }

    @Override
    public RegistrationDTO getRegistrationById(Integer id) {
        return registrationMapper.getRegistrationById(id);
    }

    @Override
    public RegistrationDTO createRegistration(RegistrationDTO registrationDTO) {
        registrationMapper.createRegistration(registrationDTO);
        return registrationDTO;
    }

    @Override
    public RegistrationDTO updateRegistration(Integer id, RegistrationDTO registrationDTO) {
        registrationDTO.setId(id);
        registrationMapper.updateRegistration(registrationDTO);
        return registrationDTO;
    }

    @Override
    public void deleteRegistration(Integer id) {
        registrationMapper.deleteRegistration(id);
    }

}
