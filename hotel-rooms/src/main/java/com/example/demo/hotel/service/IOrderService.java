package com.example.demo.hotel.service;

import com.example.demo.hotel.DTO.OrderDTO;

import java.util.List;

public interface IOrderService {

     List<OrderDTO> getAllOrder();
     OrderDTO getOrderById(Integer id);
     OrderDTO createOrder(OrderDTO orderDTO);
     OrderDTO updateOrder(Integer id, OrderDTO orderDTO);
     void deleteOrder(Integer id);

}
