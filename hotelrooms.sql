/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : hotelrooms

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 15/11/2023 18:19:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for orderinfo
-- ----------------------------
DROP TABLE IF EXISTS `orderinfo`;
CREATE TABLE `orderinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ordernumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单编号',
  `roomnumber` int(11) NOT NULL COMMENT '房间号码',
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '宾客姓名',
  `checkindate` datetime(0) NOT NULL COMMENT '入住日期',
  `sumprice` int(10) NOT NULL COMMENT '总金额',
  `paystatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderinfo
-- ----------------------------
INSERT INTO `orderinfo` VALUES (32, 'f3f99c40da7', 5010, '112233', '2023-05-13 17:36:37', 8888, '已支付', '1');
INSERT INTO `orderinfo` VALUES (30, '5820d2e03a1', 2009, '222', '2023-10-20 17:28:50', 368, '已支付', '111');
INSERT INTO `orderinfo` VALUES (31, '5e7200d5dae', 5005, '4444', '2023-11-13 17:35:51', 8888, '已支付', '1');
INSERT INTO `orderinfo` VALUES (28, '392c35735be', 1001, '李建成', '2023-09-13 16:51:56', 288, '已支付', '无');
INSERT INTO `orderinfo` VALUES (29, '47a23446c7a', 1001, 'aaa', '2023-11-15 17:21:25', 288, '已支付', '111');
INSERT INTO `orderinfo` VALUES (33, '3dd39c7bc76', 2008, '王大锤', '2023-01-10 17:37:54', 368, '已支付', '');
INSERT INTO `orderinfo` VALUES (34, 'a704732704c', 4003, 'aaaa', '2023-11-07 17:38:51', 698, '已支付', '1');
INSERT INTO `orderinfo` VALUES (35, 'fca843ede47', 4007, 'yy', '2023-05-13 17:39:52', 698, '已支付', '1');
INSERT INTO `orderinfo` VALUES (36, '02cad8eb8d7', 3008, 'dd', '2023-06-13 17:40:37', 588, '已支付', '1');
INSERT INTO `orderinfo` VALUES (37, 'ba7d19a2b51', 1006, 'uu', '2023-09-13 21:28:41', 288, '已支付', '1');
INSERT INTO `orderinfo` VALUES (38, '40fe902566f', 2003, 'zz', '2023-11-13 21:29:19', 368, '已支付', '1');
INSERT INTO `orderinfo` VALUES (39, 'acb1b1a7aa0', 5001, 'sss', '2023-03-13 21:29:49', 8888, '已支付', '1');
INSERT INTO `orderinfo` VALUES (40, 'b38776d1bff', 2005, '1212', '2023-11-13 22:42:30', 368, '已支付', '1');
INSERT INTO `orderinfo` VALUES (41, '6ea892caccd', 1005, '212', '2023-04-15 16:26:06', 288, '已支付', '1');
INSERT INTO `orderinfo` VALUES (42, '92367f93983', 1003, '121', '2023-05-15 16:26:29', 288, '已支付', '1');
INSERT INTO `orderinfo` VALUES (43, '11490313f2b', 1008, '1121', '2023-07-15 16:26:51', 288, '已支付', '1');
INSERT INTO `orderinfo` VALUES (44, 'a493eb1ec09', 3004, '121', '2023-11-15 16:27:12', 588, '已支付', '1');
INSERT INTO `orderinfo` VALUES (45, '75d8b46ded1', 3006, '2121', '2023-04-15 16:27:34', 588, '已支付', '1');
INSERT INTO `orderinfo` VALUES (46, '2b68fb42e49', 3009, '1221', '2023-11-15 16:27:59', 588, '已支付', '1');
INSERT INTO `orderinfo` VALUES (47, '04183f892dd', 2002, 'eqwe', '2023-07-15 16:28:26', 368, '已支付', '1');
INSERT INTO `orderinfo` VALUES (48, '00e5be91145', 2007, '3232', '2023-02-15 16:28:51', 368, '已支付', '11');
INSERT INTO `orderinfo` VALUES (49, '71e1e26abeb', 3002, '123', '2023-02-15 16:29:37', 588, '已支付', '11212');
INSERT INTO `orderinfo` VALUES (50, 'ab745f329bf', 4006, '2121', '2023-01-15 16:56:04', 698, '已支付', '11');
INSERT INTO `orderinfo` VALUES (51, 'f33e7ac6b2e', 4002, '122', '2023-04-11 00:00:00', 698, '已支付', '1');
INSERT INTO `orderinfo` VALUES (52, 'fbbe8d9012e', 2008, '4343', '2023-08-18 00:00:00', 368, '已支付', '1');
INSERT INTO `orderinfo` VALUES (54, '1de83032419', 5006, '2121', '2023-08-18 00:00:00', 8888, '已支付', '1');
INSERT INTO `orderinfo` VALUES (55, 'f6e79e57e77', 4008, '3232', '2023-11-15 17:26:04', 698, '已支付', '1');

-- ----------------------------
-- Table structure for registrationinfo
-- ----------------------------
DROP TABLE IF EXISTS `registrationinfo`;
CREATE TABLE `registrationinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `roomnumber` int(11) NOT NULL COMMENT '房间号码',
  `roomtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '房间类型',
  `price` int(11) NOT NULL COMMENT '房间价格',
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '住户姓名',
  `userid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '身份证号码',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '住户手机号码',
  `checkindate` datetime(0) NOT NULL COMMENT '入住日期',
  `days` int(11) NOT NULL COMMENT '入住天数',
  `deposit` int(11) NOT NULL COMMENT '预收押金',
  `sumprice` int(11) NOT NULL COMMENT '合计金额',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of registrationinfo
-- ----------------------------
INSERT INTO `registrationinfo` VALUES (20, 1001, '标准单间', 288, '李建成', '452627200013987265', '19330418888', '2023-08-16 17:14:40', 1, 500, 788, '无');
INSERT INTO `registrationinfo` VALUES (21, 1001, '标准单间', 288, 'aaa', '111111111111111111', '19330418898', '2023-10-26 17:27:55', 1, 500, 788, '111');
INSERT INTO `registrationinfo` VALUES (22, 2009, '双人房', 368, '222', '111111111111111111', '19330418857', '2023-11-13 17:28:50', 1, 500, 868, '111');
INSERT INTO `registrationinfo` VALUES (24, 5010, '总统套房', 8888, '112233', '222222222222222222', '19330418756', '2023-02-15 17:36:37', 1, 500, 9388, '1');
INSERT INTO `registrationinfo` VALUES (25, 2008, '双人房', 368, '王大锤', '555555555555555555', '19330418535', '2023-11-13 17:37:59', 2, 500, 1236, '22');
INSERT INTO `registrationinfo` VALUES (26, 4003, '三人房', 698, 'aaaa', '555555555555555555', '19330418857', '2023-11-13 17:38:51', 1, 500, 1198, '1');
INSERT INTO `registrationinfo` VALUES (27, 4007, '三人房', 698, 'yy', '434324444444444444', '19330418755', '2023-11-13 17:39:52', 1, 500, 1198, '1');
INSERT INTO `registrationinfo` VALUES (28, 3008, '豪华大床房', 588, 'dd', '454543532423434234', '19330418857', '2023-11-13 17:40:37', 1, 500, 1088, '1');
INSERT INTO `registrationinfo` VALUES (29, 1006, '标准单间', 288, 'uu', '898988988989889989', '19330418857', '2023-11-13 21:28:41', 1, 500, 788, '1');
INSERT INTO `registrationinfo` VALUES (30, 2003, '双人房', 368, 'zz', '343434325453434322', '19330415698', '2023-11-13 21:29:19', 1, 500, 868, '1');
INSERT INTO `registrationinfo` VALUES (32, 2005, '双人房', 368, '1212', '321322131221121231', '19330418857', '2023-11-13 22:42:30', 1, 500, 868, '1');
INSERT INTO `registrationinfo` VALUES (33, 1005, '标准单间', 288, '212', '432434324324233322', '19330415555', '2023-11-15 16:26:06', 1, 500, 788, '1');
INSERT INTO `registrationinfo` VALUES (34, 1003, '标准单间', 288, '121', '122121212121212121', '19330415555', '2023-11-15 16:26:29', 1, 500, 788, '1');
INSERT INTO `registrationinfo` VALUES (35, 1008, '标准单间', 288, '1121', '212121121212121212', '19330415555', '2023-11-15 16:26:51', 1, 500, 788, '1');
INSERT INTO `registrationinfo` VALUES (36, 3004, '豪华大床房', 588, '121', '232131212121212123', '19330415555', '2023-11-15 16:27:12', 1, 500, 1088, '1');
INSERT INTO `registrationinfo` VALUES (37, 3006, '豪华大床房', 588, '2121', '121211211211211121', '19330415555', '2023-11-15 16:27:34', 1, 500, 1088, '1');
INSERT INTO `registrationinfo` VALUES (38, 3009, '豪华大床房', 588, '1221', '213213123121221312', '19330415555', '2023-11-15 16:27:59', 1, 500, 1088, '1');
INSERT INTO `registrationinfo` VALUES (39, 2002, '双人房', 368, 'eqwe', '312321342121213213', '19330415555', '2023-11-15 16:28:26', 1, 500, 868, '1');
INSERT INTO `registrationinfo` VALUES (40, 2007, '双人房', 368, '3232', '213123232312321321', '19330415555', '2023-11-15 16:28:51', 1, 500, 868, '11');
INSERT INTO `registrationinfo` VALUES (41, 3002, '豪华大床房', 588, '123', '213131231232313223', '19330415555', '2023-11-15 16:29:37', 1, 500, 1088, '11212');
INSERT INTO `registrationinfo` VALUES (42, 4006, '三人房', 698, '2121', '432423423422323423', '19330418857', '2023-11-15 16:56:04', 1, 500, 1198, '11');
INSERT INTO `registrationinfo` VALUES (43, 4002, '三人房', 698, '122', '442343242323223423', '19330418857', '2023-04-11 00:00:00', 1, 500, 1198, '1');
INSERT INTO `registrationinfo` VALUES (44, 2008, '双人房', 368, '4343', '545454435453533453', '19330418857', '2023-08-18 00:00:00', 1, 500, 868, '1');
INSERT INTO `registrationinfo` VALUES (45, 5006, '总统套房', 8888, '2121', '434233243244342232', '19330418888', '2023-08-18 00:00:00', 1, 500, 9388, '1');
INSERT INTO `registrationinfo` VALUES (46, 4008, '三人房', 698, '3232', '234234343243442343', '19330418547', '2023-11-15 17:26:04', 2, 500, 1896, '1');

-- ----------------------------
-- Table structure for roomsinfo
-- ----------------------------
DROP TABLE IF EXISTS `roomsinfo`;
CREATE TABLE `roomsinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `roomnumber` int(11) NOT NULL COMMENT '房间号码',
  `roomtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '房间类型',
  `price` int(11) DEFAULT NULL COMMENT '房间价格',
  `bedtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '床型',
  `wlantype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '宽带类型',
  `deposit` int(11) DEFAULT NULL COMMENT '押金标准',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `roomnumber`(`roomnumber`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomsinfo
-- ----------------------------
INSERT INTO `roomsinfo` VALUES (1, 1001, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (2, 1002, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (3, 1003, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (4, 1004, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (5, 1005, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (6, 1006, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (7, 1007, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (8, 1008, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (9, 1009, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (10, 1010, '标准单间', 288, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (11, 2001, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (12, 2002, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (13, 2003, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (14, 2004, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (15, 2005, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (16, 2006, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (17, 2007, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (18, 2008, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (19, 2009, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (20, 2010, '双人房', 368, '双床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (21, 3001, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (22, 3002, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (23, 3003, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (24, 3004, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (25, 3005, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (26, 3006, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (27, 3007, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (28, 3008, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (29, 3009, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (30, 3010, '豪华大床房', 588, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (31, 4001, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (32, 4002, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (33, 4003, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (34, 4004, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (35, 4005, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (36, 4006, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (37, 4007, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (38, 4008, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (39, 4009, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (40, 4010, '三人房', 698, '三床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (41, 5001, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (42, 5002, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (43, 5003, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (44, 5004, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (45, 5005, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (46, 5006, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (47, 5007, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (48, 5008, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (49, 5009, '总统套房', 8888, '大床', '免费使用', 500, '无');
INSERT INTO `roomsinfo` VALUES (50, 5010, '总统套房', 8888, '大床', '免费使用', 500, '无');

-- ----------------------------
-- Table structure for roomstatusinfo
-- ----------------------------
DROP TABLE IF EXISTS `roomstatusinfo`;
CREATE TABLE `roomstatusinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `roomnumber` int(11) NOT NULL COMMENT '房间号码',
  `roomtype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '房间类型',
  `price` int(11) NOT NULL COMMENT '房间价格',
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '入住客户',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '房间状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roomstatusinfo
-- ----------------------------
INSERT INTO `roomstatusinfo` VALUES (10, 1001, '标准单间', 288, '李建成', '19330418898', '入住', '111');
INSERT INTO `roomstatusinfo` VALUES (9, 1002, '标准单间', 288, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (8, 1003, '标准单间', 288, '121', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (7, 1004, '标准单间', 288, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (6, 1005, '标准单间', 288, '212', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (11, 1006, '标准单间', 288, 'uu', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (12, 1007, '标准单间', 288, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (13, 1008, '标准单间', 288, '王大锤', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (14, 1009, '标准单间', 288, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (15, 1010, '标准单间', 288, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (16, 2001, '双人房', 368, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (17, 2002, '双人房', 368, 'eqwe', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (18, 2003, '双人房', 368, 'zz', '19330415698', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (19, 2004, '双人房', 368, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (20, 2005, '双人房', 368, '1212', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (21, 2006, '双人房', 368, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (22, 2007, '双人房', 368, '3232', '19330415555', '入住', '11');
INSERT INTO `roomstatusinfo` VALUES (23, 2008, '双人房', 368, '张三', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (24, 2009, '双人房', 368, '222', '19330418857', '入住', '111');
INSERT INTO `roomstatusinfo` VALUES (25, 2010, '双人房', 368, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (26, 3001, '豪华大床房', 588, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (27, 3002, '豪华大床房', 588, '123', '19330415555', '入住', '11212');
INSERT INTO `roomstatusinfo` VALUES (28, 3003, '豪华大床房', 588, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (29, 3004, '豪华大床房', 588, '121', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (30, 3005, '豪华大床房', 588, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (31, 3006, '豪华大床房', 588, '2121', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (32, 3007, '豪华大床房', 588, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (33, 3008, '豪华大床房', 588, 'dd', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (34, 3009, '豪华大床房', 588, '小美', '19330415555', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (35, 3010, '豪华大床房', 588, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (36, 4001, '三人房', 698, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (37, 4002, '三人房', 698, '122', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (38, 4003, '三人房', 698, '小帅', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (39, 4004, '三人房', 698, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (40, 4005, '三人房', 698, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (41, 4006, '三人房', 698, '2121', '19330418857', '入住', '11');
INSERT INTO `roomstatusinfo` VALUES (42, 4007, '三人房', 698, 'yy', '19330418755', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (43, 4008, '三人房', 698, '佛博乐', '19330418547', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (44, 4009, '三人房', 698, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (45, 4010, '三人房', 698, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (46, 5001, '总统套房', 8888, 'sss', '19330145888', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (47, 5002, '总统套房', 8888, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (48, 5003, '总统套房', 8888, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (49, 5004, '总统套房', 8888, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (50, 5005, '总统套房', 8888, '4444', '19330418857', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (51, 5006, '总统套房', 8888, '2121', '19330418888', '入住', '1');
INSERT INTO `roomstatusinfo` VALUES (52, 5007, '总统套房', 8888, '', '', '清洁', '');
INSERT INTO `roomstatusinfo` VALUES (53, 5008, '总统套房', 8888, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (54, 5009, '总统套房', 8888, '', '', '空闲', '');
INSERT INTO `roomstatusinfo` VALUES (55, 5010, '总统套房', 8888, '112233', '19330418756', '入住', '1');

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `sex` enum('男','女','保密') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '性别',
  `role` enum('超级管理员','管理员','普通用户') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '普通用户' COMMENT '角色',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` enum('禁用','启用') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES (1, 'admin', '123456', '李建成', '男', '超级管理员', '19330418888', '2812584440@qq.com', '启用', '无');
INSERT INTO `userinfo` VALUES (2, '1', '1', '王大锤', '女', '管理员', '19330418851', '2812584440@qq.com', '启用', '111');
INSERT INTO `userinfo` VALUES (3, 'user1', '123456', '张三', '男', '普通用户', '13812345678', 'user1@example.com', '启用', '这是用户1的备注');
INSERT INTO `userinfo` VALUES (5, 'user2', '123456', '王五', '男', '普通用户', '13666666666', 'user2@example.com', '启用', '这是用户2的备注');
INSERT INTO `userinfo` VALUES (6, 'user3', '123456', '赵六', '女', '普通用户', '13999999999', 'user3@example.com', '启用', '这是用户3的备注');
INSERT INTO `userinfo` VALUES (8, 'user4', '123456', '孙八', '男', '普通用户', '13333333333', 'user4@example.com', '禁用', '这是用户4的备注');
INSERT INTO `userinfo` VALUES (9, 'user5', '123456', '周九', '女', '普通用户', '13222222222', 'user5@example.com', '启用', '这是用户5的备注');

-- ----------------------------
-- View structure for view_order
-- ----------------------------
DROP VIEW IF EXISTS `view_order`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_order` AS select date_format(`months`.`month`,'%c月') AS `month`,coalesce(sum(`hotelrooms`.`orderinfo`.`sumprice`),0) AS `totalprice` from (((select ('2023-01-01' + interval (`numbers`.`n` - 1) month) AS `month` from (select 1 AS `n` union select 2 AS `2` union select 3 AS `3` union select 4 AS `4` union select 5 AS `5` union select 6 AS `6` union select 7 AS `7` union select 8 AS `8` union select 9 AS `9` union select 10 AS `10` union select 11 AS `11` union select 12 AS `12`) `numbers`)) `months` left join `hotelrooms`.`orderinfo` on(((month(`hotelrooms`.`orderinfo`.`checkindate`) = month(`months`.`month`)) and (year(`hotelrooms`.`orderinfo`.`checkindate`) = year(`months`.`month`))))) group by `months`.`month` order by `months`.`month`;

-- ----------------------------
-- View structure for view_roomstatus
-- ----------------------------
DROP VIEW IF EXISTS `view_roomstatus`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_roomstatus` AS select `roomstatusinfo`.`roomtype` AS `roomtype`,sum((case when (`roomstatusinfo`.`status` = '空闲') then 1 else 0 end)) AS `vacant`,sum((case when (`roomstatusinfo`.`status` = '入住') then 1 else 0 end)) AS `occupied`,sum((case when (`roomstatusinfo`.`status` = '清洁') then 1 else 0 end)) AS `cleaned` from `roomstatusinfo` group by `roomstatusinfo`.`roomtype`;

-- ----------------------------
-- View structure for view_roomtype
-- ----------------------------
DROP VIEW IF EXISTS `view_roomtype`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_roomtype` AS select `registrationinfo`.`roomtype` AS `roomtype`,sum(`registrationinfo`.`price`) AS `total_price` from `registrationinfo` where (year(`registrationinfo`.`checkindate`) = 2023) group by `registrationinfo`.`roomtype`;

SET FOREIGN_KEY_CHECKS = 1;
