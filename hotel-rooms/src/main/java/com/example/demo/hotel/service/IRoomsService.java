package com.example.demo.hotel.service;

import com.example.demo.hotel.DTO.RoomsDTO;

import java.util.List;

public interface IRoomsService {

     List<RoomsDTO> getAllRooms();
     RoomsDTO getRoomsById(Integer id);
     RoomsDTO createRooms(RoomsDTO roomsDTO);
     RoomsDTO updateRooms(Integer id, RoomsDTO roomsDTO);
     void deleteRooms(Integer id);

}
