package com.example.demo.hotel.controller;

import com.example.demo.hotel.mapper.ViewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/views")
@CrossOrigin(origins = "http://localhost:8080/", methods = {RequestMethod.GET, RequestMethod.POST})
public class ViewController {

    @Autowired
    private ViewMapper viewMapper;

    @GetMapping("/order")
    public List<Map<String, Object>> viewOrder() {
        return viewMapper.getViewOrder();
    }

    @GetMapping("/roomstatus")
    public List<Map<String, Object>> viewRoomStatus() {
        return viewMapper.getViewRoomStatus();
    }

    @GetMapping("/roomtype")
    public List<Map<String, Object>> viewRoomType() {
        return viewMapper.getViewRoomType();
    }
}
