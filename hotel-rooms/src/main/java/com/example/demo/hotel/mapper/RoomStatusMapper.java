package com.example.demo.hotel.mapper;

import com.example.demo.hotel.DTO.RoomStatusDTO;

import java.util.List;

//@Mapper
public interface RoomStatusMapper {

    List<RoomStatusDTO> getAllRoomStatus();

    RoomStatusDTO getRoomStatusById(Integer id);

    void createRoomStatus(RoomStatusDTO roomStatusDTO);

    void updateRoomStatus(RoomStatusDTO roomStatusDTO);

    void deleteRoomStatus(Integer id);


    void updateRoomStatusByRoomNumber(RoomStatusDTO roomStatusDTO);//根据房间号更改房间状态

}
