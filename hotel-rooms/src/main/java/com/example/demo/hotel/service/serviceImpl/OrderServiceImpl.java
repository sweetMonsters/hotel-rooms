package com.example.demo.hotel.service.serviceImpl;

import com.example.demo.hotel.DTO.OrderDTO;
import com.example.demo.hotel.mapper.OrderMapper;
import com.example.demo.hotel.service.IOrderService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {
    private final OrderMapper orderMapper;

    public OrderServiceImpl(OrderMapper orderMapper) {
        this.orderMapper = orderMapper;
    }

    @Override
    public List<OrderDTO> getAllOrder() {
        return orderMapper.getAllOrder();
    }

    @Override
    public OrderDTO getOrderById(Integer id) {
        return orderMapper.getOrderById(id);
    }

    @Override
    public OrderDTO createOrder(OrderDTO orderDTO) {
        orderMapper.createOrder(orderDTO);
        return orderDTO;
    }

    @Override
    public OrderDTO updateOrder(Integer id, OrderDTO orderDTO) {
        orderDTO.setId(id);
        orderMapper.updateOrder(orderDTO);
        return orderDTO;
    }

    @Override
    public void deleteOrder(Integer id) {
        orderMapper.deleteOrder(id);
    }

}
