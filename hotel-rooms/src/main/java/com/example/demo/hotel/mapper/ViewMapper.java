package com.example.demo.hotel.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface ViewMapper {

    @Select("SELECT * FROM view_order")
    List<Map<String, Object>> getViewOrder();

    @Select("SELECT * FROM view_roomstatus")
    List<Map<String, Object>> getViewRoomStatus();

    @Select("SELECT * FROM view_roomtype")
    List<Map<String, Object>> getViewRoomType();
}
