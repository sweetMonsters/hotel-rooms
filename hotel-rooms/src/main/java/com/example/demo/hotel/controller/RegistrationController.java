package com.example.demo.hotel.controller;

import com.example.demo.hotel.DTO.RegistrationDTO;
import com.example.demo.hotel.service.IRegistrationService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/registrationinfo")
@CrossOrigin(origins = "http://localhost:8080/", methods = {RequestMethod.GET, RequestMethod.POST})
public class RegistrationController {
    private final IRegistrationService registrationService;

    public RegistrationController(IRegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    /**
     * getAll
     * @return
     */
    @GetMapping("/get")
    public List<RegistrationDTO> getAllRegistration() {
        return registrationService.getAllRegistration();
    }

    /**
     * getByID
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public RegistrationDTO getRegistrationById(@PathVariable Integer id) {
        return registrationService.getRegistrationById(id);
    }


    /**
     * add
     * @param registrationDTO
     * @return
     */
    @PostMapping("/add")
    public RegistrationDTO createRegistration(@RequestBody RegistrationDTO registrationDTO) {
        return registrationService.createRegistration(registrationDTO);
    }

    /**
     * update
     * @param id
     * @param registrationDTO
     * @return
     */
    @PutMapping("/update/{id}")
    public RegistrationDTO updateRegistration(@PathVariable Integer id, @RequestBody RegistrationDTO registrationDTO) {
        return registrationService.updateRegistration(id, registrationDTO);
    }

    /**
     * delete
     * @param id
     */
    @DeleteMapping("/delete/{id}")
    public void deleteRegistration(@PathVariable Integer id) {
        registrationService.deleteRegistration(id);
    }

}
