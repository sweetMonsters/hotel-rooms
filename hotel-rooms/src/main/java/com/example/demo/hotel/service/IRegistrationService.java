package com.example.demo.hotel.service;

import com.example.demo.hotel.DTO.RegistrationDTO;

import java.util.List;

public interface IRegistrationService {

     List<RegistrationDTO> getAllRegistration();
     RegistrationDTO getRegistrationById(Integer id);
     RegistrationDTO createRegistration(RegistrationDTO registrationDTO);
     RegistrationDTO updateRegistration(Integer id, RegistrationDTO registrationDTO);
     void deleteRegistration(Integer id);

}
