package com.example.demo.hotel.service.serviceImpl;

import com.example.demo.hotel.DTO.UserDTO;
import com.example.demo.hotel.mapper.UserMapper;
import com.example.demo.hotel.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    private final UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userMapper.getAllUsers();
    }

    @Override
    public UserDTO getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        userMapper.createUser(userDTO);
        return userDTO;
    }

    @Override
    public UserDTO updateUser(Integer id, UserDTO userDTO) {
        userDTO.setId(id);
        userMapper.updateUser(userDTO);
        return userDTO;
    }

    @Override
    public void deleteUser(Integer id) {
        userMapper.deleteUser(id);
    }


    @Override
    public boolean validateUser(String account, String password) {
        UserDTO user = userMapper.findByUsername(account);
        if (user != null && user.getPassword().equals(password)) {
            return true;
        }
        return false;
    }


}
