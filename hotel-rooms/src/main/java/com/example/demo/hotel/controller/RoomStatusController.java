package com.example.demo.hotel.controller;

import com.example.demo.hotel.DTO.RoomStatusDTO;
import com.example.demo.hotel.service.IRoomStatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roomstatus")
@CrossOrigin(origins = "http://localhost:8080/", methods = {RequestMethod.GET, RequestMethod.POST})
public class RoomStatusController {
    private final IRoomStatusService roomStatusService;

    public RoomStatusController(IRoomStatusService roomStatusService) {
        this.roomStatusService = roomStatusService;
    }

    /**
     * getAll
     * @return
     */
    @GetMapping("/get")
    public List<RoomStatusDTO> getAllRoomStatus() {
        return roomStatusService.getAllRoomStatus();
    }

    /**
     * getByID
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public RoomStatusDTO getRoomStatusById(@PathVariable Integer id) {
        return roomStatusService.getRoomStatusById(id);
    }


    /**
     * add
     * @param roomStatusDTO
     * @return
     */
    @PostMapping("/add")
    public RoomStatusDTO createRoomStatus(@RequestBody RoomStatusDTO roomStatusDTO) {
        return roomStatusService.createRoomStatus(roomStatusDTO);
    }

    /**
     * update
     * @param id
     * @param roomStatusDTO
     * @return
     */
    @PutMapping("/update/{id}")
    public RoomStatusDTO updateRoomStatus(@PathVariable Integer id, @RequestBody RoomStatusDTO roomStatusDTO) {
        return roomStatusService.updateRoomStatus(id, roomStatusDTO);
    }

    /**
     * delete
     * @param id
     */
    @DeleteMapping("/delete/{id}")
    public void deleteRoomStatus(@PathVariable Integer id) {
        roomStatusService.deleteRoomStatus(id);
    }


    @PutMapping("/status/{roomnumber}")
    public ResponseEntity<String> updateRoomStatusByRoomNumber(
            @PathVariable Integer roomnumber,
            @RequestBody RoomStatusDTO roomStatusDTO) {
        roomStatusDTO.setRoomnumber(roomnumber);
        roomStatusService.updateRoomStatusByRoomNumber(roomStatusDTO);
        return ResponseEntity.ok("Room status updated successfully");
    }

}
