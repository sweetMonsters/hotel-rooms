package com.example.demo.hotel.service;

import com.example.demo.hotel.DTO.RoomStatusDTO;

import java.util.List;

public interface IRoomStatusService {

     List<RoomStatusDTO> getAllRoomStatus();
     RoomStatusDTO getRoomStatusById(Integer id);
     RoomStatusDTO createRoomStatus(RoomStatusDTO roomStatusDTO);
     RoomStatusDTO updateRoomStatus(Integer id, RoomStatusDTO roomStatusDTO);
     void deleteRoomStatus(Integer id);


     void updateRoomStatusByRoomNumber(RoomStatusDTO roomStatusDTO);//根据房间号更改房间状态

}
