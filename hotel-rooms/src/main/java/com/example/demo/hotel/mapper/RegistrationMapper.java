package com.example.demo.hotel.mapper;

import com.example.demo.hotel.DTO.RegistrationDTO;

import java.util.List;

//@Mapper
public interface RegistrationMapper {

    List<RegistrationDTO> getAllRegistration();

    RegistrationDTO getRegistrationById(Integer id);

    void createRegistration(RegistrationDTO registrationDTO);

    void updateRegistration(RegistrationDTO registrationDTO);

    void deleteRegistration(Integer id);

}
